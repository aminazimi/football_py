import http.client
import mimetypes
import json
import datetime
import team_controller as team_cl
import league_controller as league_cl
import match
import match_controller as match_cl
import match_time_controller as match_time_cl
import match_time
import score_controller as score_cl
import score
import match_assistant_controller as match_assitant_cl
import matchassistant
import coverage_controller as coverage_cl
import coverage
import standing_controller as standing_cl
import standing

class football_data:
    def __init__(self,host_name):
        self.__host_name = host_name

    def call_api(self,api_url):
        conn = http.client.HTTPSConnection(self.__host_name)
        payload = ''
        # conn.request("GET", "/v2.2/livescores/?user=sahandsamaneh88&token=ea81af0cbcc9c51acc0cd75e51cdb2bd&t=today", payload)
        # conn.request("GET", "/v2.2/fixtures/?user=sahandsamaneh88&token=ea81af0cbcc9c51acc0cd75e51cdb2bd&t=schedule&d=2020/07/08", payload)
        conn.request("GET", api_url, payload)
        res = conn.getresponse()
        data_api = res.read()
        # print(data_api.decode("utf-8"))

        file = open('soccerapi_today.json', 'w')
        file.write(data_api.decode("utf-8"))
        file.close()

        # with open("soccerapi_today.json",encoding="utf-8") as fh :
        #     data = json.load(fh)
        # # data = json.load(data_api.decode("utf-8"))
        # print("converted")
        data= json.loads(data_api.decode("utf-8"))

        if data["meta"]["count"] > 0:
            home_team_id =0
            away_team_id=0
            try:
                for data_node in data["data"]:
                    #process home team
                    home_team = data_node["teams"]["home"]
                    team_controller= team_cl.TeamController(home_team["id"])
                    team_records = team_controller.get_team_by_source_id()
                    if team_records==None:
                        home_team_id= team_controller.add_team(home_team["name"],home_team["short_code"],home_team["img"],home_team["form"],home_team["coach_id"],home_team["color"])
                    else:
                        home_team_id = team_records[0]
                    #process away team
                    away_team = data_node["teams"]["away"]
                    team_controller= team_cl.TeamController(away_team["id"])
                    team_records = team_controller.get_team_by_source_id()
                    if team_records==None:
                        home_team_id= team_controller.add_team(away_team["name"],away_team["short_code"],away_team["img"],away_team["form"],away_team["coach_id"],away_team["color"])
                    else:
                        away_team_id = team_records[0]
                        print("away:"+str(home_team_id))
                    #process league
                    league=data_node["league"]
                    league_class = league_cl.LeagueController(league["id"])
                    league_records = league_class.get_league_by_source_id()
                    if league_records == None:
                        league_id = league_class.add_league(league["name"],league["type"],league["country_id"],league["country_name"],league["country_flag"])
                        print("ins leagu:"+str(league_id))
                    else:
                        league_id = league_records[0]
                    print("league id:{0}".format(league_id))
                    #process Match
                    match_class = match_cl.MatchController(data_node["id"])
                    match_records=match_class.get_match_by_source_id()
                    if match_records == None:
                        _match = match.Match
                        _match.status = data_node["status"]
                        _match.status_name =data_node["status_name"]
                        _match.status_period = data_node["status_period"]
                        _match.referee_id =  data_node["referee_id"]
                        _match.round_id =   data_node["round_id"]
                        _match.season_id=data_node["season_id"]
                        _match.season_name = data_node["season_name"]
                        _match.stage_id= data_node["stage_id"]
                        _match.stage_name=data_node["stage_name"]
                        _match.group_id=data_node["group_id"]
                        _match.group_name=data_node["group_name"]
                        _match.aggregate_id=data_node["aggregate_id"]
                        _match.winner_team_id=data_node["winner_team_id"]
                        _match.venue_id= data_node["venue_id"]
                        _match.leg= data_node["leg"]
                        _match.team_home_id= home_team_id
                        _match.team_away_id =away_team_id
                        _match.source_id = data_node["id"]
                        _match.league_id= league_id
                        match_id= match_class.add_match(_match)
                    else:
                        match_id = match_records[0]
                    #process match time
                    matchTime_class = match_time_cl.MatchTimeController()
                    matchTime_records = matchTime_class.get_match_time_by_matchId(match_id) 
                    if matchTime_records == None:
                        mtchTime=data_node["time"]
                        _match_time = match_time.match_time
                        _match_time.match_datetime = mtchTime["datetime"]
                        _match_time.minutes= mtchTime["minute"]
                        _match_time.timezone= mtchTime["timezone"]
                        _match_time.match_id= match_id
                        match_time_id = matchTime_class.add_match_time(_match_time)
                    else:
                        match_time_id = matchTime_class.get_match_time_by_matchId(match_id)
                    #process scores
                    score_class = score_cl.ScoreController()
                    score_records = score_class.get_score_by_match_id(match_id)
                    if score_records == None:
                        score_node= data_node["scores"]
                        _score = score.score
                        _score.home_score = score_node["home_score"]
                        _score.away_score = score_node["away_score"] 
                        _score.ht_score = score_node["ht_score"] 
                        _score.et_score = score_node["et_score"] 
                        _score.ft_score = score_node["ft_score"] 
                        _score.ps_score = score_node["ps_score"] 
                        _score.match_id = match_id 
                        score_id = score_class.add_score(_score)
                    else:
                        score_id = score_records[0]
                    #process assitans
                    match_assistant_class = match_assitant_cl.MatchAssistantController()
                    match_assistant_records = match_assistant_class.get_match_assitant_by_match_id(match_id)
                    if match_assistant_records==None:
                        match_assistant_node = data_node["assistants"]
                        _assitant = matchassistant.matchAssistant
                        _assitant.first_assistant_id = match_assistant_node["first_assistant_id"]
                        _assitant.second_assistant_id = match_assistant_node["second_assistant_id"]
                        _assitant.fourth_assistant_id = match_assistant_node["fourth_assistant_id"]
                        _assitant.match_id = match_id
                        match_assistant_id = match_assistant_class.add_match_assitant(_assitant)
                    else:
                        match_assistant = match_assistant_records[0]
                    #process coverage
                    coverage_class = coverage_cl.CoverageController()
                    coverage_records = coverage_class.get_covergae_by_match_id(match_id)
                    if coverage_records == None:
                        coverage_node =  data_node["coverage"]
                        _coverage = coverage.coverage
                        _coverage.has_lineups = coverage_node["has_lineups"]
                        _coverage.has_tvs = coverage_node["has_tvs"]
                        _coverage.match_id = match_id
                        coverage_id = coverage_class.add_coverage(_coverage)
                    else:
                        coverage_id = coverage_records[0]
                    #process standing
                    standing_class = standing_cl.StandingController()
                    standing_records = standing_class.get_standing_by_match_id(match_id)
                    if standing_records == None:
                        standing_node =  data_node["standings"]
                        _standing = standing.standing
                        _standing.home_position = standing_node["home_position"]
                        _standing.away_position = standing_node["away_position"]
                        _standing.match_id =match_id
                        standing_id = standing_class.add_standing(_standing)
                    else:
                        standing_id = standing_records[0]


            except Exception as e:
                file = open('footballX_engine.error', 'a')
                file.write( "{0}, Error {1} \n".format(str(datetime.datetime.now()), str(e)))
                file.close()
                print("error: "+e)
        else:
            print("no data!!")
